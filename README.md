PD-Tools
========

PD-Tools is a collection of programs made to be used in conjunction with the
[Phantom Drive game engine](https://bitbucket.org/leodmanx2/phantom-drive-wt).

Tools
------

### PDCONV

PDCONV is a 3D model format conversion tool. It takes exported interchange
format files like COLLADA or Wavefront OBJ and writes them to the engine's
internal format. PDCONV uses Assimp internally, so you can find a list of
supported formats by checking their website.

In the future, PDCONV should also be able to convert the internal format to a
variety of interchange formats.

### PDGEN

PDGEN is used to generate physics models from render models. It can take the
output of PDCONV along with a mass and output another file that can be used by
the Phantom Drive engine to do things such as collision checking.
