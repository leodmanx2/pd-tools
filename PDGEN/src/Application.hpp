#ifndef APPLICATION_H
#define APPLICATION_H

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "pphy.hpp"
#include <pmdl.hpp>

class Application {
	private:
	po::variables_map       m_varMap;
	po::options_description m_optionsDesc;

	public:
	Application(int argc, char** argv);

	void run();
};

#endif
