#include "Application.hpp"

// TODO: Convert PMDL to other formats

Application::Application(int argc, char** argv)
  : m_varMap(), m_optionsDesc("Options") {
	m_optionsDesc.add_options()("help,h", "produce help message")(
	  "input,i", po::value<std::string>(), "input file")(
	  "output,o", po::value<std::string>(), "output file");

	po::positional_options_description inputOption;
	inputOption.add("input", -1);

	po::command_line_parser parser(argc, argv);
	parser.options(m_optionsDesc);
	parser.positional(inputOption);

	po::store(parser.run(), m_varMap);
	po::notify(m_varMap);
}

void Application::run() {
	if(m_varMap.empty() || m_varMap.count("help")) {
		std::cout << m_optionsDesc << "\n";
	} else if(m_varMap.count("input")) {
		std::string inFile  = m_varMap["input"].as<std::string>();
		std::string outFile = m_varMap.count("output") ?
		                        m_varMap["output"].as<std::string>() :
		                        "model.mdl";
		// TODO: Process!
		// We need to convert the render model into a vertex-only model. We can
		// build the convex hull here, but it's not necessary.
	} else {
		throw std::runtime_error("Input file needed");
	}
}

int main(int argc, char** argv) {
	try {
		Application app(argc, argv);
		app.run();
	} catch(const std::exception& e) {
		std::clog << e.what();
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
