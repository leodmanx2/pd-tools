#ifndef PMDL_H
#define PMDL_H

#include <cmath>
#include <cstdint>
#include <fstream>
#include <limits>

#include <cereal/archives/json.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/vector.hpp>

namespace PMDL {

	// -----------------------------------------------------------------------------
	//  Basic types
	// -----------------------------------------------------------------------------

	using uint8 = std::uint8_t;

	using uint32 = std::uint32_t;
	using int32  = std::int32_t;

	using float32 = float;

	// Using basic floating point types relies on them having fixed precision
	// (32, 64 bits) and following a single standard (IEE 754).
	// Cereal also does this check internally, but we'll do them anyway.
	static_assert(std::numeric_limits<float>::is_iec559,
	              "Float type is not IEEE 754");

	// -----------------------------------------------------------------------------
	//  Vectors
	// -----------------------------------------------------------------------------

	struct Vec3f {
		float32 x;
		float32 y;
		float32 z;

		template <typename Archive>
		void serialize(Archive& archive) {
			archive(CEREAL_NVP(x), CEREAL_NVP(y), CEREAL_NVP(z));
		}
	};

	// -----------------------------------------------------------------------------
	//  File structures
	// -----------------------------------------------------------------------------

	struct Header {
		uint32 signature;
		uint32 version;

		Header() : signature(), version(){};
		Header(const uint32 sig, const uint8 vers)
		  : signature(sig), version(vers) {}

		template <typename Archive>
		void serialize(Archive& archive) {
			archive(CEREAL_NVP(signature), CEREAL_NVP(version));
		}
	};

	struct Body {
		std::vector<Vec3f> points;
		float32            mass;

		Body() : points(), mass(){};
		Body(const std::vector<Vec3f>& p, const float32 m) : points(p), mass(m) {}

		template <typename Archive>
		void serialize(Archive& archive) {
			archive(CEREAL_NVP(points), CEREAL_NVP(mass));
		}
	};

	struct File {
		Header header;
		Body   body;

		File() : header(), body(){};
		File(const Header& head, const Body& bod) : header(head), body(bod) {}

		void write(const std::string& filename) {
			std::ofstream                       of(filename, std::ofstream::binary);
			cereal::PortableBinaryOutputArchive oarchive(of);
			oarchive(*this);
		}

		static File parse(std::istream& fileContents) {
			cereal::PortableBinaryInputArchive iarchive(fileContents);
			File                               file;
			iarchive(file);
			return file;
		}

		template <typename Archive>
		void serialize(Archive& archive) {
			archive(CEREAL_NVP(header), CEREAL_NVP(body));
		}
	};
}

#endif
