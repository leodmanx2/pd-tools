The PMDL File Format
====================

**Version 1**

This document describes the *PMDL* three-dimensional model file format intended
to be used as part of the *Phantom Drive* video game engine.

This document is *official*, meaning applications are expected to conform to
the structure defined herein and not the other way around.

Header
------

**Byte-Order Mark**† : 1 byte

*   0x00 if big-endian  
*   0x01 if little-endian  

**File-Type Signature** : 4 bytes

*   PMDL if B.O.M = 0x00
*   LDMP if B.O.M = 0x01

**Version**‡ : 4 byte

Body
----

**Number of Vertices** : 8 bytes

**Vertices** : 32 bytes each

*   **Position** : 12 bytes

    *   **x** : 4 bytes  
    *   **y** : 4 bytes  
    *   **z** : 4 bytes

*   **Normal** : 12 bytes

    *   **x** : 4 bytes
    *   **y** : 4 bytes
    *   **z** : 4 bytes

*   **Texture Coordinate** : 8 bytes

    *   **u** : 4 bytes
    *   **v** : 4 bytes

**Number of Indices** : 8 bytes

**Indices** : 4 bytes each

Notes
-----

†The B.O.M is a product of using the Cereal serialization library
compounded with laziness. It's not necessary but since it makes PDCONV
implementation a little easier it has wormed its way into the formal
specification.

‡This number refers to the version number at the top of this document.
