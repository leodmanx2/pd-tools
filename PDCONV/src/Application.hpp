#ifndef APPLICATION_H
#define APPLICATION_H

#include <exception>
#include <fstream>
#include <iostream>
#include <tuple>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <pmdl.hpp>

using VertexIndexTuple =
  std::tuple<std::vector<PMDL::Vertex>, std::vector<PMDL::Index>>;

class Application {
	private:
	po::variables_map       m_varMap;
	po::options_description m_optionsDesc;

	VertexIndexTuple importMesh(const std::string& inFile);
	void exportMesh(const std::string&        outFile,
	                std::vector<PMDL::Vertex> vertices,
	                std::vector<PMDL::Index>  indices);

	public:
	Application(int argc, char** argv);
	~Application();

	void run();
};

#endif
