#include "Application.hpp"

// TODO: Convert PMDL to other formats

Application::Application(int argc, char** argv)
  : m_varMap(), m_optionsDesc("Options") {
	m_optionsDesc.add_options()("help,h", "produce help message")(
	  "input,i", po::value<std::string>(), "input file")(
	  "output,o", po::value<std::string>(), "output file")(
	  "flip-texcoords,f",
	  "Flip texture coordinates to be compatible with OpenGL applications");

	po::positional_options_description inputOption;
	inputOption.add("input", -1);

	po::command_line_parser parser(argc, argv);
	parser.options(m_optionsDesc);
	parser.positional(inputOption);

	po::store(parser.run(), m_varMap);
	po::notify(m_varMap);
}

Application::~Application() {}

void Application::run() {
	if(m_varMap.empty() || m_varMap.count("help")) {
		std::cout << m_optionsDesc << "\n";
	} else if(m_varMap.count("input")) {
		std::string inFile  = m_varMap["input"].as<std::string>();
		std::string outFile = m_varMap.count("output") ?
		                        m_varMap["output"].as<std::string>() :
		                        "model.mdl";
		auto mesh     = importMesh(inFile);
		auto vertices = std::get<0>(mesh);
		auto indices  = std::get<1>(mesh);
		exportMesh(outFile, vertices, indices);
	} else {
		throw std::runtime_error("Input file needed");
	}
}

VertexIndexTuple Application::importMesh(const std::string& inFile) {
	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(
	  inFile,
	  aiProcess_CalcTangentSpace | aiProcess_GenSmoothNormals |
	    aiProcess_JoinIdenticalVertices | aiProcess_ImproveCacheLocality |
	    aiProcess_LimitBoneWeights | aiProcess_RemoveRedundantMaterials |
	    aiProcess_Triangulate | aiProcess_GenUVCoords | aiProcess_SortByPType |
	    aiProcess_FindInvalidData | aiProcess_FindInstances |
	    aiProcess_ValidateDataStructure | aiProcess_OptimizeMeshes |
	    aiProcess_Debone);
	if(!scene) throw std::runtime_error(importer.GetErrorString());

	if(scene->HasAnimations())
		std::clog << "Warning: Scene has animations but thay are not supported.\n";
	if(scene->HasCameras())
		std::clog << "Warning: Scene has cameras but thay are not supported.\n";
	if(scene->HasLights())
		std::clog << "Warning: Scene has lights but thay are not supported.\n";
	if(scene->HasMaterials())
		std::clog << "Warning: Scene has materials but thay are not supported.\n";
	if(scene->HasTextures())
		std::clog << "Warning: Scene has textures but thay are not supported.\n";
	if(!scene->HasMeshes())
		throw std::runtime_error("Scene does not contain any meshes.");
	if(scene->mNumMeshes > 1) {
		std::stringstream ss;
		ss << "Scene contains more than one mesh. Number of meshes: ";
		ss << scene->mNumMeshes;
		throw std::runtime_error(ss.str());
	}

	auto mesh = scene->mMeshes[0];

	if(mesh->HasBones())
		std::clog << "Warning: Mesh has bones but thay are not supported yet.\n";
	if(mesh->GetNumUVChannels() > 1)
		std::clog << "Warning: Mesh has more than one UV channel.\n";
	if(!mesh->HasNormals())
		throw std::runtime_error("Mesh does not contain normals");
	if(!mesh->HasTextureCoords(0))
		throw std::runtime_error("Mesh does not contain texture coordinates");

	std::vector<PMDL::Vertex> vertices;
	std::vector<PMDL::Index>  indices;

	for(size_t i = 0; i < mesh->mNumVertices; ++i) {
		PMDL::Vec3f pos;
		pos.x = mesh->mVertices[i].x;
		pos.y = mesh->mVertices[i].y;
		pos.z = mesh->mVertices[i].z;

		PMDL::Vec3f norm;
		norm.x = mesh->mNormals[i].x;
		norm.y = mesh->mNormals[i].y;
		norm.z = mesh->mNormals[i].z;

		PMDL::Vec2f uv;
		uv.u = mesh->mTextureCoords[0][i].x;
		uv.v = m_varMap.count("flip-texcoords") ? 1 - mesh->mTextureCoords[0][i].y :
		                                          mesh->mTextureCoords[0][i].y;

		vertices.push_back(PMDL::Vertex(pos, norm, uv));
	}

	for(size_t i = 0; i < mesh->mNumFaces; ++i) {
		if(mesh->mFaces[i].mNumIndices != 3)
			throw std::runtime_error("Discovered non-triangle face.");

		for(size_t j = 0; j < 3; j++) {
			indices.push_back(mesh->mFaces[i].mIndices[j]);
		}
	}

	return std::make_tuple(vertices, indices);
}

void Application::exportMesh(const std::string&        outFile,
                             std::vector<PMDL::Vertex> vertices,
                             std::vector<PMDL::Index>  indices) {
	PMDL::Header header(0x504d444c, 1); // 0x504d444c = "PMDL"
	PMDL::Body   body(vertices, indices);
	PMDL::File   file(header, body);

	file.write(outFile);
}

int main(int argc, char** argv) {
	try {
		Application app(argc, argv);
		app.run();
	} catch(const std::exception& e) {
		std::clog << e.what();
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
